
const sqlite3 = require("sqlite3").verbose();

export function createDatabase(logger) {
    // application
    let db = new sqlite3.Database("./server.db", (err) => {
        if (err) {
            return logger.error(err);
        }
        logger.info("Connected to the in-memory SQlite database.");
    });

    db.serialize(function () {
        // create table devices
        // device id : autommaticaly generated id for the device (UUID) : prevent other people form firguring out too easly
        db.run("CREATE TABLE if not exists Devices (id INTEGER NOT NULL PRIMARY KEY, DeviceName TEXT UNIQUE, DeviceID TEXT, DeviceToken NOT NULL TEXT)");

        // create table commands
        // device name : id of the computer host
        // username : id of the user having asked for this request
        // client id : id of the device having with wich the user has resquest this command
        // i.e : the device to wich we must give the result
        db.run("CREATE TABLE if not exists Commands (CommandID INTEGER NOT NULL PRIMARY KEY, DeviceID TEXT, Command TEXT NOT NULL, Result TEXT, SendDate TEXT, ResultDate TEXT, Username TEXT NOT NULL, Executed BOOLEAN)");
    });
    return db;
}