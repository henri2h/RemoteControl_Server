import { generateUUIDv4, getData } from "./js/server";
import { createExpress, runExpress, setError, setLogger, queryCallback } from "./js/expressLogic.js";
import { createCustomLogger } from "./js/logger";
import { createDatabase } from "./js/databaseLogic";

const logger = createCustomLogger();

// update libraries
setLogger(logger);

// detect if first time run
// first run ?
if (getData() === "created") {
    var text = "\n";
    text += "####################################################\n";
    text += "#  data.json created : Please fill and restart...  #\n";
    text += "####################################################\n";
    text += " \n";

    // eslint-disable-next-line no-console
    console.log(text);

    process.exit();
}

var db = createDatabase(logger);
var app = createExpress(logger);


// client
app.get("/getDevices", (req, res, next) => {
    var sql = "SELECT * FROM Devices";
    var params = [];
    db.all(sql, params, (err, rows) => queryCallback(err, rows, res));
});

// get commands
app.post("/getCommands", (req, res, next) => {
    var sql = "SELECT * FROM Commands WHERE DeviceID = ? AND Executed = 0";
    var params = [req.body.id];
    db.all(sql, params, (err, rows) => queryCallback(err, rows, res));
});

app.post("/getAllCommands", (req, res, next) => {
    var sql = "select * from Commands where DeviceID = ?";
    var params = [req.body.id];
    db.all(sql, params, (err, rows) => queryCallback(err, rows, res));
});

// add a command
app.post("/addCommand", (req, res, next) => {
    var sql = "INSERT INTO Commands (DeviceID, Command, SendDate, Username, Executed) VALUES (?, ?, ?, ?, 0)";

    // get username
    var username = req.body.username;
    // TODO : check if the username should have access to this computer (device_id)

    var sendDate = new Date().getTime().toString(); // get time

    var params = [req.body.id, req.body.command, sendDate, username];
    db.run(sql, params, (err, rows) => queryCallback(err, rows, res));
});



// machine
app.post("/setCommandResult", (req, res, next) => {
    var sql = "UPDATE Commands SET Result = ?, ResultDate = ?, Executed = 1 WHERE CommandID = ?";
    var resultDate = new Date().getTime().toString(); // get time

    var params = [req.body.result, resultDate, req.body.commandID];
    db.run(sql, params, (err, result) => {
        if (err) {
            setError(err, res);
        }
        res.json({
            "message": "success",
            "id": this.lastID
        })
    });
});


// add device
app.post("/addDevice/", (req, res, next) => {
    var errors = [];
    if (!req.body.name) {
        errors.push("No name specified");
    }
    if (errors.length) {
        res.status(400).json({ "success": false, "error": errors.join(",") });
        return;
    }
    var data = {
        name: req.body.name
    };

    var token = generateUUIDv4();

    var sql = "INSERT INTO devices (DeviceName, DeviceToken) VALUES (?,?)";
    var params = [data.name, token];

    // send back data to device
    data.token = token;

    db.run(sql, params, function (err, result) {
        if (err) {
            setError(err, res);
            return;
        }
        res.json({
            "message": "success",
            "data": data,
            "id": this.lastID, // not really usefull.. to delete
            token
        });
    });
});


runExpress(app, logger);

// should close the database
// db.close();
